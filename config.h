/**
 * config.h
 *
 */

#ifndef CONFIG_H
#define CONFIG_H

#ifndef TOSH_DATA_LENGTH
#	define TOSH_DATA_LENGTH 28
#endif

#ifdef NET_TELOSB_DEBUG 
#	include "printf.h"
#	define debug(fmt, ...) printf("[debug] %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__ )
#	define debug_nofile(fmt, ...) printf("[debug] " fmt "\n", ##__VA_ARGS__)
#	define debug_and_flush(...) \
	do {	\
		debug(__VA_ARGS__);	\
		printfflush();	\
	} while(0)
#	define debug_flush() printfflush()

//#define RAM_DEBUG
//#define ROUTING_DEBUG
#else
#	define debug(...) 
#	define debug_nofile(...) 
#	define debug_and_flush(...) 
#	define debug_flush()
#endif

#endif /* CONFIG_H */

