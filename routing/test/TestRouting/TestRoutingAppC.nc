#include "config.h"

configuration TestRoutingAppC {

}
implementation {
	components MainC;
	components RoutingC;
	components TestRoutingP;
	components new TimerMilliC() as Timer;
	components ActiveMessageC as AM;
#ifdef NET_TELOSB_DEBUG
	components PrintfC, InitDebugC;
#endif
	
	TestRoutingP.Boot -> MainC;
	TestRoutingP.Routing -> RoutingC;
	TestRoutingP.Timer -> Timer;
	TestRoutingP.AMControl -> AM;
	TestRoutingP.AMPacket -> AM;

}
