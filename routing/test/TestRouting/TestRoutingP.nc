#include "config.h"

module TestRoutingP {
	uses {
		interface Boot;
		interface Routing;
		interface Timer<TMilli>;
		interface SplitControl as AMControl;
		interface AMPacket;
	}
}
implementation {
	
	event void Boot.booted() {
		call AMControl.start();
	}

	event void AMControl.startDone(error_t err) {
		if (err == SUCCESS) {
			call Routing.init();
			call Timer.startPeriodic(1000);
		}
		else {
			call AMControl.start();
		}
	}

	event void AMControl.stopDone(error_t err) {
	}

	task void query_routing_info() {
		am_addr_t next_hop;
		uint8_t dis;
		am_addr_t i;
		am_addr_t myaddr = call AMPacket.address();

		debug("I am %u", myaddr);
		for (i = 0; i < 3; ++i) {
			if (SUCCESS != call Routing.get_next_hop(i, &next_hop)) {
				debug("error when invoking get_next_hop(%u)", i);
				continue;
			}
			if (SUCCESS != call Routing.get_distance(i, &dis)) {
				debug("error when invoking get_distance(%u)", i);
				continue;
			}
			debug("dest = %u, next_hop = %u, distance = %u", i, next_hop, dis);
		}
		debug_flush();
	}

	event void Timer.fired() {
		post query_routing_info();
	}
	
}

