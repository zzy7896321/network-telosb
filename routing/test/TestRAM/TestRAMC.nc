#include "config.h"
#include <TinyError.h>
#include <AM.h>

module TestRAMC {
	uses {
		interface Boot;
		interface Leds;
		interface Timer<TMilli> as Timer0;
		interface Timer<TMilli> as Timer1;
		interface Timer<TMilli> as Timer2;
		interface Timer<TMilli> as SendTimer;
		interface AMSend;
		interface Receive;
		interface AMPacket;
		interface Packet;
		interface SplitControl as RAMControl;
	}
}
implementation {
	am_addr_t next_to_send;
	uint32_t magic_number = 0x19940504u;
	message_t msg;
	
	event void Boot.booted() {
		call RAMControl.start();
	}

	event void RAMControl.startDone(error_t error) {
		next_to_send = call AMPacket.address();
		call SendTimer.startPeriodic(1000);
	}

	event void RAMControl.stopDone(error_t error) {
	}

	event void SendTimer.fired() {
		nx_uint32_t *payload = (nx_uint32_t *) 
			call Packet.getPayload(&msg, sizeof(nx_uint32_t));
		*payload = magic_number;
		call AMSend.send(next_to_send, &msg, sizeof(nx_uint32_t));
		if (++next_to_send == 3) next_to_send = 0;
	}

	event void Timer0.fired() {
		call Leds.led0Off();
	}

	event void Timer1.fired() {
		call Leds.led1Off();
	}

	event void Timer2.fired() {
		call Leds.led2Off();
	}

	event void AMSend.sendDone(message_t *m, error_t error) {}

	event message_t *Receive.receive(message_t *m, void *payload, uint8_t len) {
		if (len == sizeof(nx_uint32_t)) {
			uint32_t val = *(nx_uint32_t *) payload;
			if (val == magic_number) {
				am_addr_t src = call AMPacket.source(m);
				if (src >= 0 && src < 3) {
					if (src == 0) {
						call Leds.led0On();
						call Timer0.startOneShot(500);
					}
					else if (src == 1) {
						call Leds.led1On();
						call Timer1.startOneShot(500);
					}
					else if (src == 2) {
						call Leds.led2On();
						call Timer2.startOneShot(500);
					}
				}
			}
		}
		return m;
	}

}
