
#include "config.h"

configuration TestRAMAppC {

}
implementation {
	components MainC, LedsC;
	components new RoutedAMSendC(15) as Send;
	components new RoutedAMReceiverC(15) as Receive;
	components new TimerMilliC() as Timer;
	
	components new TimerMilliC() as Timer0;
	components new TimerMilliC() as Timer1;
	components new TimerMilliC() as Timer2;
	components RoutedActiveMessageC;
	components TestRAMC as TestRAM;

#ifdef NET_TELOSB_DEBUG
	components PrintfC, InitDebugC;
#endif

	TestRAM.Boot -> MainC;
	TestRAM.Leds -> LedsC;
	TestRAM.Timer0 -> Timer0;
	TestRAM.Timer1 -> Timer1;
	TestRAM.Timer2 -> Timer2;

	TestRAM.SendTimer -> Timer;
	TestRAM.AMSend -> Send;
	TestRAM.Receive -> Receive;
	TestRAM.AMPacket -> Send;
	TestRAM.Packet -> Send;
	TestRAM.RAMControl -> RoutedActiveMessageC;
}

