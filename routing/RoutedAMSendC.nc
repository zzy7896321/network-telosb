
#include "config.h"
#include <AM.h>
#include <TinyError.h>

generic configuration RoutedAMSendC(uint8_t AMId) {
	provides {
		interface AMSend;
		interface Packet;
		interface AMPacket;
	}
}
implementation {
	components RoutedActiveMessageC;

	AMSend = RoutedActiveMessageC.AMSend[AMId];
	Packet = RoutedActiveMessageC;
	AMPacket = RoutedActiveMessageC;
}
