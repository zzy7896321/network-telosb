#ifndef ROUTING_H
#define ROUTING_H

#include "config.h"
#include "utils.h"
#include <AM.h>

enum { 
	AM_ROUTING = 0x1f,
	AM_ROUTEDMSG = 0x2f
};

enum { FAST_HEARTBEAT_INTERVAL_MS = 1000 }; /* 1 per second */
enum { SLOW_FAST_RATIO = 3 };
enum { SLOW_HEARTBEAT_INTERVAL_MS = FAST_HEARTBEAT_INTERVAL_MS * SLOW_FAST_RATIO }; /* 1 per 5 seconds */
enum { MAX_MISS_HEARTBEAT = 6 }; /* time out after 15 / FAST_HEARTBEAT_INTERVAL seconds */
enum { MAX_NUM_HOSTS = 16 };

enum { ROUTING_RECV_BUF_SIZE = 4 };

#define INVALID_HOST_ADDR ((am_addr_t) MAX_NUM_HOSTS)
#define INF_DISTANCE ((uint8_t) MAX_NUM_HOSTS)

typedef struct host_t {
	am_addr_t	h_addr;
	am_addr_t	h_nexthop;
	uint8_t		h_distance;
	uint16_t	h_last_heartbeat;

	bool		h_updated;
} host_t;

typedef nx_struct routing_msg_header_t {
	nx_uint8_t		rm_magic;	
	nx_uint8_t		rm_updated;		/* number of updated hosts */
} routing_msg_header_t;

/*
 * NOTE: higher 4 bits are the am_address of the host;
 *		lower 4 bits are the distance - 1 
 *		
 *		We never need to send node with distance 0 (i.e. self)
 *		to other nodes since it is implicitly represented by the
 *		heartbeat message. Therefore, we can safely substract 1
 *		from the distance without underflow. Since the maximum distance
 *		is 15, we can represent inf with the value of 16. 16 - 1 = 15
 *		fits in the lower 4 bits of rm_hostinfo.
 *
 */
typedef nx_struct routing_msg_t {
	routing_msg_header_t	rm_header;
	nx_uint8_t				rm_hostinfo[MAX_NUM_HOSTS]; /* see NOTE in the comment above*/
} routing_msg_t;

static_assert_decl(sizeof(routing_msg_t) <= TOSH_DATA_LENGTH, "TOSH_DATA_LEN is too small to fit routing msgs");

#define ROUTING_MSG_MAGIC 0x4a
#define ROUTING_MSG_ADDR_MASK 0xf0
#define ROUTING_MSG_ADDR_SHIFT 4
#define ROUTING_MSG_DIS_MASK 0x0f
#define ROUTING_MSG_DIS_SHIFT 0

#define ROUTING_MSG_HOSTINFO_OFFSET (size_t) _offsetof(routing_msg_t, rm_hostinfo)
#define ROUTING_MSG_HOSTINFO_SIZE sizeof(((routing_msg_t*)0)->rm_hostinfo[0])

#define ROUTING_MSG_GET_ADDR(hostinfo) ((am_addr_t)((hostinfo) >> ROUTING_MSG_ADDR_SHIFT))
#define ROUTING_MSG_GET_DIS(hostinfo) (((uint8_t)((hostinfo) & ROUTING_MSG_DIS_MASK)) + 1)

#define ROUTING_MSG_GET_SIZE(nhosts) \
	(size_t) ((ROUTING_MSG_HOSTINFO_OFFSET) + nhosts * (ROUTING_MSG_HOSTINFO_SIZE))

typedef nx_struct routed_packet_header_t {
	nx_uint8_t	type;
	nx_uint8_t	srcdst;
} routed_packet_header_t;

static_assert_decl(sizeof(routed_packet_header_t) <= TOSH_DATA_LENGTH, "TOSH_DATA_LENGTH is too small to fit packet header");

#define PACKET_HEADER_GET_SRC(pkt_header) (((pkt_header)->srcdst) >> 4)
#define PACKET_HEADER_GET_DST(pkt_header) (((pkt_header)->srcdst) & 0xf)
#define PACKET_HEADER_SET_SRC(pkt_header, src) \
	(pkt_header)->srcdst = (PACKET_HEADER_GET_DST(pkt_header) | (((src) & 0xf)<< 4))
#define PACKET_HEADER_SET_DST(pkt_header, dst)	\
	(pkt_header)->srcdst = (((pkt_header->srcdst) & ~0xf) | ((dst) & 0xf))
#define PACKET_HEADER_SET_SRCDST(pkt_header, src, dst)	\
	(pkt_header)->srcdst = ((((src) & 0xf) << 4) | ((dst) & 0xf))

#define ROUTED_MSG_QUEUE_SIZE 8
#define ROUTED_MSG_BUFFER_SIZE 6

#endif
