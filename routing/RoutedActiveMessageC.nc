/* RoutedActiveMessageC */

#include "config.h"
#include "routing.h"
#include <AM.h>

configuration RoutedActiveMessageC {
	provides {
		interface AMPacket;
		interface AMSend[uint8_t id];
		interface Receive[uint8_t id];
		interface Packet;
		interface SplitControl;
	}
}
implementation {
	components RoutingC;
	components RoutedActiveMessageP as RAM;
	components ActiveMessageC as AM;
	components new QueueC(message_t *, ROUTED_MSG_QUEUE_SIZE) as q1;
	components new QueueC(message_t *, ROUTED_MSG_QUEUE_SIZE) as q2;
	components new QueueC(message_t *, ROUTED_MSG_QUEUE_SIZE) as q3;
	
	RAM.Routing -> RoutingC;
	RAM.SubSend -> AM.AMSend[AM_ROUTEDMSG];
	RAM.SubReceive -> AM.Receive[AM_ROUTEDMSG];
	RAM.SubAMPacket -> AM;
	RAM.SubPacket -> AM;
	RAM.SubSplitControl -> AM;
	RAM.PacketAcknowledgements -> AM;

	RAM.send_queue -> q1;
	RAM.deliver_queue -> q2;
	RAM.cancel_queue -> q3;

	AMPacket = RAM;
	AMSend = RAM;
	Receive = RAM;
	Packet = RAM;
	SplitControl = RAM;
}

