/**
 * routing/Routing.nc
 *
 */


#include "config.h"
#include "routing.h"
#include <TinyError.h>
#include <AM.h>

#ifdef ROUTING_DEBUG
#	define routing_debug(...) debug_nofile(__VA_ARGS__)
#	define routing_debug_and_flush(...)	\
		do {	\
			debug_nofile(__VA_ARGS__);	\
			debug_flush();	\
		} while(0)
#	define routing_debug_nofile(...) debug_nofile(__VA_ARGS__)
#	define routing_debug_flush() debug_flush()

#	define routing_debug_tblentry_fmt "%4u%4u%4u%8u%2d"
#	define routing_debug_tblentry(i) \
		routing_debug(routing_debug_tblentry_fmt,	\
			m_host[i].h_addr, m_host[i].h_nexthop,	\
			m_host[i].h_distance, m_host[i].h_last_heartbeat,	\
			m_host[i].h_updated)
#	define routing_debug_tblentry_with_info(i, additional_fmt, ...)	\
		routing_debug(routing_debug_tblentry_fmt "\t" additional_fmt,	\
			m_host[i].h_addr, m_host[i].h_nexthop,	\
			m_host[i].h_distance, m_host[i].h_last_heartbeat,	\
			m_host[i].h_updated, __VA_ARGS__)

#else
#	define routing_debug(...) 
#	define routing_debug_and_flush(...) 
#	define routing_debug_nofile(...) 
#	define routing_debug_flush() 

#	define routing_debug_tblentry_fmt
#	define routing_debug_tblentry(...)
#	define routing_debug_tblentry_with_info(...)
#endif

#define ROUTING_QUEUE_NEXT(i) QUEUE_NEXT(i, ROUTING_RECV_BUF_SIZE)
#define ROUTING_QUEUE_PREV(i) QUEUE_PREV(i, ROUTING_RECV_BUF_SIZE)
#define ROUTING_QUEUE_GO_NEXT(i) QUEUE_GO_NEXT(i, ROUTING_RECV_BUF_SIZE)
#define ROUTING_QUEUE_GO_PREV(i) QUEUE_GO_PREV(i, ROUTING_RECV_BUF_SIZE)

module RoutingP {
	provides {
		interface Routing;
	}
	uses {
		interface AMPacket;
		interface AMSend;
		interface Receive;
		interface Timer<TMilli> as heartbeat_timer;
		interface Random;

		interface Leds;
	}
}
implementation {
	bool		m_started = FALSE;
	am_addr_t	m_myaddr;
	host_t		m_host[MAX_NUM_HOSTS];

	bool		m_sending;
	message_t	m_send_buf;

	message_t	m_recv_buf[ROUTING_RECV_BUF_SIZE];
	message_t	*m_recv_freelist[ROUTING_RECV_BUF_SIZE];
	uint8_t		m_recv_nfree;
	
	uint8_t		m_proc_head;
	uint8_t		m_proc_tail;
	message_t	*m_proc_queue[ROUTING_RECV_BUF_SIZE + 1];
	
	bool		m_fast_heartbeat;
	uint16_t	m_heartbeat_no;

	nx_uint8_t create_hostinfo(am_addr_t addr, uint8_t distance) {
		return (((nx_uint8_t) (addr & 0x0f)) << 4) | ((distance - 1) & 0x0f);
	}

	bool addr_is_valid(am_addr_t addr) {
		return addr < MAX_NUM_HOSTS;
	}

	am_addr_t my_addr() {
		return call AMPacket.address();
	}

	void schedule_next_heartbeat(bool fast) {
		if (!m_started) return ;
		call heartbeat_timer.startOneShot((m_fast_heartbeat = fast)
				? FAST_HEARTBEAT_INTERVAL_MS : SLOW_HEARTBEAT_INTERVAL_MS);
	}

	void set_host_invalid(am_addr_t host) {
		m_host[host].h_addr = INVALID_HOST_ADDR;
		m_host[host].h_distance = INF_DISTANCE;
		m_host[host].h_nexthop = INVALID_HOST_ADDR;
		m_host[host].h_updated = TRUE;
	}

	bool check_is_timeout(am_addr_t peer) {
		if (!addr_is_valid(m_host[peer].h_addr)) return TRUE;
		if (m_heartbeat_no - m_host[peer].h_last_heartbeat > MAX_MISS_HEARTBEAT) {
			set_host_invalid(peer);
			return TRUE;
		}
		return FALSE;
	}

	bool check_host_is_alive(am_addr_t host) {
		if (m_host[host].h_nexthop != host) {
			if (!addr_is_valid(m_host[host].h_addr)) {
				return FALSE; 
			}

			if (check_is_timeout(m_host[host].h_nexthop)) {
				set_host_invalid(host);
				return FALSE;
			}
			return TRUE;
		}
		else {
			return !check_is_timeout(host);
		}
	}

	command void Routing.init() {
		int i;

		atomic m_started = TRUE;

		m_myaddr = my_addr();

		if (!addr_is_valid(m_myaddr)) {
			return ;
		}

		for (i = 0; i < MAX_NUM_HOSTS; ++i) {
			m_host[i].h_addr = INVALID_HOST_ADDR;
			m_host[i].h_distance = INF_DISTANCE;
			m_host[i].h_nexthop = INVALID_HOST_ADDR;
			m_host[i].h_last_heartbeat = 0;
			m_host[i].h_updated = FALSE;
		}
		m_host[m_myaddr].h_addr = m_myaddr;
		m_host[m_myaddr].h_nexthop = m_myaddr;
		m_host[m_myaddr].h_distance = 0;
		m_host[m_myaddr].h_last_heartbeat = 0;

		m_sending = FALSE;
		
		for (i = 0; i < ROUTING_RECV_BUF_SIZE; ++i) {
			m_recv_freelist[i] = &m_recv_buf[i];
		}
		m_recv_nfree = ROUTING_RECV_BUF_SIZE;
		
		m_proc_head = m_proc_tail = 0;
		
		m_heartbeat_no = 0;
		schedule_next_heartbeat(TRUE);
		
		return ;
	}

	command void Routing.stop() {
		atomic m_started = FALSE;
	}

	command error_t Routing.get_next_hop(am_addr_t dest, am_addr_t *next_hop) {
		if (!addr_is_valid(dest))
			return EINVAL;
		if (!next_hop)
			return EINVAL;
		
		check_host_is_alive(dest);
		*next_hop = m_host[dest].h_nexthop;

		return SUCCESS;
	}

	command error_t Routing.get_distance(am_addr_t dest, uint8_t *distance) {
		if (!addr_is_valid(dest))
			return EINVAL;
		if (!distance)
			return EINVAL;
		
		check_host_is_alive(dest);
		*distance = m_host[dest].h_distance;

		return SUCCESS;
	}
	
	task void send_heartbeat() {
		bool fast, updated = FALSE;
		int i;
		uint8_t len;
		routing_msg_t *rmsg;
		error_t error;

		m_heartbeat_no += (m_fast_heartbeat) ? 1 : SLOW_FAST_RATIO;
		m_host[m_myaddr].h_last_heartbeat = m_heartbeat_no;

		rmsg = call AMSend.getPayload(&m_send_buf, sizeof(routing_msg_t));
		if (!(fast = (m_sending || rmsg == NULL))) {
			m_sending = TRUE;
			
			rmsg->rm_header.rm_magic = ROUTING_MSG_MAGIC;
			rmsg->rm_header.rm_updated = 0;

			routing_debug("send heartbeat, routing table: ");

			for (i = 0; i < MAX_NUM_HOSTS; ++i) {
				if (m_host[i].h_addr != m_myaddr && 
					(check_host_is_alive(i) || m_host[i].h_updated)) {

					rmsg->rm_hostinfo[rmsg->rm_header.rm_updated++] =
						create_hostinfo((am_addr_t) i, m_host[i].h_distance);
				}

				routing_debug_tblentry(i);

				updated |= m_host[i].h_updated;
				m_host[i].h_updated = FALSE;
			}

			len = ROUTING_MSG_GET_SIZE(rmsg->rm_header.rm_updated);
			routing_debug("nupdated = %u, len = %u", rmsg->rm_header.rm_updated, len);

			if (SUCCESS != (error = call AMSend.send(AM_BROADCAST_ADDR, &m_send_buf, len))) {
				fast = TRUE;
				m_sending = FALSE;
				routing_debug_and_flush("sending failed %d", error);
			}
		}
	
		fast |= updated;
		routing_debug("fast = %d, updated = %d", fast, updated);
		routing_debug_flush();

		schedule_next_heartbeat(fast);
	}

	event void heartbeat_timer.fired() {
		if (m_sending) {
			routing_debug_and_flush("send conflict, schedule fast");
			schedule_next_heartbeat(TRUE);
		}
		else {
			post send_heartbeat();
		}
	}

	event void AMSend.sendDone(message_t *msg, error_t err) {
		if (err != SUCCESS) {
			routing_debug_and_flush("sending failed %d", err);
		}
		m_sending = FALSE;
	}

	task void process_recv() {
		message_t *msg;
		routing_msg_t *rmsg;
	
		msg = m_proc_queue[m_proc_head];
		ROUTING_QUEUE_GO_NEXT(m_proc_head);

		rmsg = call AMSend.getPayload(msg, ROUTING_MSG_GET_SIZE(0)); 
		if (rmsg != NULL) {
			/* be cautious */
			
			am_addr_t src = call AMPacket.source(msg);
			uint8_t nupdated = rmsg->rm_header.rm_updated;
			uint8_t tinfo;
			uint8_t i;
			am_addr_t dst;
			uint8_t dis;

			routing_debug("recv heartbeat from %u, nupdated = %u", src, nupdated);
			routing_debug_tblentry(src);
				
			for (i = 0; i < nupdated; ++i) {
				tinfo = rmsg->rm_hostinfo[i];
				dst = ROUTING_MSG_GET_ADDR(tinfo);
				dis = ROUTING_MSG_GET_DIS(tinfo);

				if (!addr_is_valid(dst)) {
					routing_debug("invalid dst, dst = %u, dis = %u", dst, dis);
				}
				
				if (dst == m_myaddr) continue;
					
				if (dis == INF_DISTANCE) {
					if (check_host_is_alive(dst) && m_host[dst].h_nexthop == src) {
						set_host_invalid(dst);	
					}
				}
				else {
					if (check_host_is_alive(dst)) {
						if (dis + 1 < m_host[dst].h_distance) {
							m_host[dst].h_nexthop = src;
							m_host[dst].h_distance = dis + 1;
							m_host[dst].h_updated = TRUE;
						}
						else if (dis + 1 == m_host[dst].h_distance) {
							m_host[dst].h_nexthop = src;
						}
					}
					else {
						m_host[dst].h_addr = dst;
						m_host[dst].h_nexthop = src;
						m_host[dst].h_distance = dis + 1;
						m_host[dst].h_updated = TRUE;
					}
				}
				routing_debug_tblentry_with_info(dst, "dst = %u, dis = %u", dst, dis);
			}

			routing_debug_flush();
		}

		m_recv_freelist[m_recv_nfree++] = msg;
		
		/* queue is not empty, continue process */
		if (m_proc_head != m_proc_tail)
			post process_recv();
	}

	event message_t *Receive.receive(message_t *msg, void *payload, uint8_t len) {
		if (len >= ROUTING_MSG_GET_SIZE(0)) {
			am_addr_t	src;
			routing_msg_t *rm = (routing_msg_t *) payload;

			if (rm->rm_header.rm_magic != ROUTING_MSG_MAGIC) {
				return msg;	
			}
			
			src = call AMPacket.source(msg);
			if (!addr_is_valid(src) || src == m_myaddr /* ??? */) {
				return msg;
			}
			
			if (!addr_is_valid(m_host[src].h_addr) || m_host[src].h_distance > 1) {
				m_host[src].h_addr = src;
				m_host[src].h_nexthop = src;
				m_host[src].h_distance = 1;
				m_host[src].h_updated = TRUE;
			}
			m_host[src].h_last_heartbeat = m_heartbeat_no;

			if (m_recv_nfree) {
				m_proc_queue[m_proc_tail] = msg;
				ROUTING_QUEUE_GO_NEXT(m_proc_tail);
				if (m_proc_tail == m_proc_head) {
					routing_debug_and_flush("queue overflow");
				}
				msg = m_recv_freelist[--m_recv_nfree];

				if (ROUTING_QUEUE_NEXT(m_proc_head) == m_proc_tail) {
					post process_recv();
				}
			}
			else {
				routing_debug_and_flush("dropping heartbeat from %u", src);
			}
		}

		return msg;
	}
}

