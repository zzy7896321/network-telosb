
#include "config.h"
#include <AM.h>

generic configuration RoutedAMReceiverC(am_id_t AmId) {
	provides {
		interface Receive;
		interface Packet;
		interface AMPacket;
	}
}
implementation {
	components RoutedActiveMessageC;

	Receive = RoutedActiveMessageC.Receive[AmId];
	Packet = RoutedActiveMessageC;
	AMPacket = RoutedActiveMessageC;
}
