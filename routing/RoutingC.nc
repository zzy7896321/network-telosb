/*
 * routing/RoutingC.nc
 *
 */
#include "config.h"
#include "routing.h"


configuration RoutingC {
	provides interface Routing;
}
implementation {
	components RoutingP;

	components new AMSenderC(AM_ROUTING);
	components new AMReceiverC(AM_ROUTING);
	components new TimerMilliC() as heartbeat_timer;
	components MainC;
	components RandomC;


	MainC.SoftwareInit -> RandomC;
	
	RoutingP.AMPacket -> AMSenderC;
	RoutingP.AMSend -> AMSenderC;
	RoutingP.Receive -> AMReceiverC;
	RoutingP.heartbeat_timer -> heartbeat_timer;
	RoutingP.Random -> RandomC;

#ifdef ROUTING_DEBUG
	components LedsC;
	RoutingP.Leds -> LedsC;
#endif

	Routing = RoutingP;
}

