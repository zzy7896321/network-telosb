/*
 * routing/Routing.nc
 *
 */

#include "config.h"
#include <TinyError.h>
#include <AM.h>

interface Routing {
	
	command void init();

	command error_t get_next_hop(am_addr_t dest, am_addr_t *next_hop);

	command error_t get_distance(am_addr_t dest, uint8_t *distance);

	command void stop();
}

