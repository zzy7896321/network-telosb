Routing protocol implementaion
===================================

## Overview

We implement a distance vector (Bellman-ford) algorithm with a maximum capacity
of 16 nodes. While the capacity of 16 is chosen arbitrarily, it is nontheless 
large enough for our sensor network which consists of only 3 to 6 nodes.

We provide the routing service through Routing interface and implement it in
RoutingC and RoutingP components. An imcomplete multi-hop active message service
based on the routing service and ActiveMessageC is implemented in RoutedActiveMessageC, 
RoutedActiveMessageP, RoutedAMSendC, RoutedAMReceiverC components.

## Routed Active Message Components

The RoutedActiveMessageC provides AMSend, Receive, Packet, AMPacket and SplitControl
interfaces based on ActiveMessageC and the routing component below.

This component is opaque to the AM layer. The type, source and destination information
is included in an additional routed message header. The layout of the message is as 
follows.

    --------------------------------------------------------
    | AM header | RM header(2 bytes) | payload | AM footer |
    --------------------------------------------------------
                |                              |
                |<--------- AM payload ------->|
                | up to TOSH_LENGTH_DATA bytes |

The RM header is formated as follows:
 
    |<---- 1 byte ---->|
    --------------------
    |      type        |
    --------------------
    |  src   |   dst   |
    --------------------

The src/dst field in the AM header is not necessary the same as those in the RM header 
because the former is the address between two hops while the latter is the initial
source and final destination along the route. So it is necessary to use AMPacket 
provided by RoutedActiveMessageC to extrace the source and destination.

Routed message are multiplexed over the same AM channel with id 0x2f. They are
multiplexed according the type field in the RM header.

Group and broadcast are not supported by routed active message. Group id is simply
passed down to AM layer. The broadcast address 0xFF is regarded as invalid address.
Spoofing and acknowledgement are also not supported.

RoutedAMSendC and RoutedAMReceiverC are counterparts of AMSenderC and AMReceiverC.

Internally, the Routed AM maintains a receiving buffer of size 6 and three queues
of size 8 for sending, cancelling and delivering messages. Since the underlying
AMSender and AMReceiver only maintains queues of size 1, there's no point to set the
size of the buffers and queues very large.

To initialize and start the service, user can use the SplitControl interface, which 
will start the AM service and initialize the routing and routed AM components. 

test/TestRAM is an application that tests the Routed AM component among 3 nodes by 
sending messages to each other in a round-robin fashion. When a node receives 
a message, it blinks the corresponding led. The messages are sent over the Routed
AM channel 15.

## Routing Components

The routing component is based on the distance vector algorithm. (See
Routing.nc, RoutingC.nc RoutingP.nc)

Each node maintains a 16-element vector of host routing information. 
The address of each node is assumed to be in range 0 (inclusive) to 
16 (exclusive). Any address greater than or equal to 16 is regarded as 
invalid address. The host information that each node maintains include 
the address of the next hop, the hop distance, the timestamp of the
last heartbeat message and an update flag(see below).

Each node broadcasts the distance vector along with a heartbeat message 
to all neighbour nodes periodically. The time interval between two
heartbeat message may be either 1 second (FAST_HEARTBEAT_INTERVAL) 
if any of the nodes is updated in last interval or 3 seconds 
(SLOW_HEARTBEAT_INTERVAL) if not. If a node does not receive heartbeat
message from a neighbour node for more than 6 seconds, the neighbour
node is considered to have left the network, which invalidates the
information about the neighbour node and any node whose next hop is the
neighbour node. All nodes that are alive at the time when the heartbeat
message is to be sent, or updated/invalidated during last interval are 
included in the heartbeat message. If two heartbeat message collide, or
sending of the heatbeat message fails, or there are updates/invalidations
during last interval, the next heartbeat message is scheduled after
FAST_HEARTBEAT_INTERVAL. Otherwise, it is scheduled after SLOW_HEARTBEAT_INTERVAL.
The timestamp of last heartbeat is approximated with the number of 
FAST_HEARTBEAT_INTERVAL expired.

The internal representation of host information is an array of host_t C-struct.
The heartbeat message payload is routing_msg_t nx_struct. See routing.h for
details.

We have a sending buffer of size 1 and a receiving buffer of size 4 for the
routing component. At most 1 slot of each them is occupied.

The AM id of the routing communcation is 0x1f.

test/TestRouting is an application that tests routing component among three nodes
with address 0, 1, 2. It periodically poll the next hop and distance information
about each of the three nodes and printf to the serial port. Add -DNET_TELOSB_DEBUG
CFLAGS to the node that is connected to the computer and 
start net.tinyos.tools.PrintfClient to see the outputs. With ROUTING_DEBUG uncommneted
complete routing table is printed periodically. But too much serial port communication
may prevent the mote from receiving/sending messages.
