/*
 * routing/RoutedActiveMessageP.nc
 */
#include "config.h"
#include "routing.h"
#include "utils.h"
#include <AM.h>
#include <TinyError.h>

#ifdef RAM_DEBUG
#	define ram_debug(...) debug_nofile(__VA_ARGS__)
#	define ram_debug_and_flush(...) \
		do {	\
			debug_nofile(__VA_ARGS__);	\
			debug_flush();	\
		} while(0)
#	define ram_debug_nofile(...) debug_nofile(__VA_ARGS__)
#	define ram_debug_flush() debug_flush()

#	define ram_msg_fmt "%-10s0x%08x%6u%6u%6u%6u"
#	define ram_debug_msg_with_info(info, msg, id, src, dst, len, additional_fmt, ...)	\
		ram_debug_and_flush(ram_msg_fmt "\t" additional_fmt, info, (unsigned) msg, id, src, dst, len, ##__VA_ARGS__)
#	define ram_debug_msg(info, msg, id, src, dst, len)	\
		ram_debug_and_flush(ram_msg_fmt, info, msg, id, src, dst, len)	
#else
#	define ram_debug(...) 
#	define ram_debug_and_flush(...) 
#	define ram_debug_nofile(...) 
#	define ram_debug_flush() 

#	define ram_debug_msg_with_info(...)	
#	define ram_debug_msg(...)
#endif

#define RAM_QUEUE_GO_NEXT(i) QUEUE_GO_NEXT(i, ROUTED_MSG_QUEUE_SIZE)
#define RAM_QUEUE_GO_PREV(i) QUEUE_GO_PREV(i, ROUTED_MSG_QUEUE_SIZE)
#define RAM_QUEUE_NEXT(i) QUEUE_NEXT(i, ROUTED_MSG_QUEUE_SIZE)
#define RAM_QUEUE_PREV(i) QUEUE_PREV(i, ROUTED_MSG_QUEUE_SIZE)

module RoutedActiveMessageP {
	provides {
		interface AMPacket;
		interface AMSend[uint8_t id];
		interface Receive[uint8_t id];
		interface Packet;
		interface SplitControl;
	}
	uses {
		interface Routing;
		interface AMSend as SubSend;
		interface Receive as SubReceive;
		interface AMPacket as SubAMPacket;
		interface Packet as SubPacket;
		interface SplitControl as SubSplitControl;
		interface PacketAcknowledgements;

		interface Queue<message_t *> as send_queue;
		interface Queue<message_t *> as deliver_queue;
		interface Queue<message_t *> as cancel_queue;
	}
}
implementation {
	int			m_buf_nfree;
	message_t	*m_buf_freelist[ROUTED_MSG_BUFFER_SIZE];
	message_t	m_buf[ROUTED_MSG_BUFFER_SIZE];

	routed_packet_header_t *get_packet_header(message_t *msg) {
		return (routed_packet_header_t *)(call SubSend.getPayload(msg, 
					sizeof(routed_packet_header_t)));
	}

	am_addr_t get_dst(message_t *msg) {
		routed_packet_header_t *packet_header = get_packet_header(msg);
		if (packet_header) {
			return PACKET_HEADER_GET_DST(packet_header);
		}
		return INVALID_HOST_ADDR;
	}

	am_addr_t get_src(message_t *msg) {
		routed_packet_header_t *packet_header = get_packet_header(msg);
		if (packet_header) {
			return PACKET_HEADER_GET_SRC(packet_header);
		}
		return INVALID_HOST_ADDR;
	}

	void get_src_dst(message_t *msg, am_addr_t *src, am_addr_t *dst) {
		routed_packet_header_t *packet_header = get_packet_header(msg);
		if (packet_header) {
			*src = PACKET_HEADER_GET_SRC(packet_header);
			*dst = PACKET_HEADER_GET_DST(packet_header);
		}
	}
	
	void set_dst(message_t *msg, am_addr_t dst) {
		routed_packet_header_t *packet_header = get_packet_header(msg);
		if (packet_header) {
			PACKET_HEADER_SET_DST(packet_header, dst);			
		}
	}

	void set_src(message_t *msg, am_addr_t src) {
		routed_packet_header_t *packet_header = get_packet_header(msg);
		if (packet_header) {
			PACKET_HEADER_SET_SRC(packet_header, src);
		}
	}

	void set_src_dst(message_t *msg, am_addr_t src, am_addr_t dst) {
		routed_packet_header_t *packet_header = get_packet_header(msg);
		if (packet_header) {
			PACKET_HEADER_SET_SRCDST(packet_header, src, dst);
		}
	}

	am_addr_t myaddr() {
		return call SubAMPacket.address();
	}

	void* get_payload(message_t *msg) {
		void *payload = call SubPacket.getPayload(msg, sizeof(routed_packet_header_t));
		return payload + sizeof(routed_packet_header_t);
	}

	uint8_t get_max_payload_len() {
		uint8_t max_len = call SubPacket.maxPayloadLength();
		return (max_len < sizeof(routed_packet_header_t)) ? 0 : 
					(max_len - sizeof(routed_packet_header_t));
	}

	uint8_t get_payload_len(message_t *msg) {
		uint8_t len = call SubPacket.payloadLength(msg);
		return (len < sizeof(routed_packet_header_t)) ? 0 : 
					(len - sizeof(routed_packet_header_t));
	}

	void set_payload_len(message_t *msg, uint8_t len) {
		call SubPacket.setPayloadLength(msg, len + sizeof(routed_packet_header_t));
	}

	uint8_t get_type(message_t *msg) {
		routed_packet_header_t *packet_header = get_packet_header(msg);
		if (packet_header) {
			return packet_header->type;
		}
		return 0;
	}

	void set_type(message_t *msg, uint8_t type) {
		routed_packet_header_t *packet_header = get_packet_header(msg);
		if (packet_header) {
			packet_header->type = type;
		}
	}

	void init() {
		m_buf_nfree = 0;
		for ( ; m_buf_nfree < ROUTED_MSG_BUFFER_SIZE; ++m_buf_nfree )
			m_buf_freelist[m_buf_nfree] = &m_buf[m_buf_nfree];
	}

	void stop() {
		/* XXX wait for queues to get empty? */
	}

	task void signal_cancel_done() {
		if (!call cancel_queue.empty()) {
			message_t *msg = call cancel_queue.dequeue();
			signal AMSend.sendDone[get_type(msg)](msg, ECANCEL);
		}
		if (!call cancel_queue.empty()) {
			post signal_cancel_done();
		}
	}
	
	task void signal_deliver() {
		if (!call deliver_queue.empty()) {
			message_t *msg = call deliver_queue.dequeue();
			am_addr_t src = get_src(msg);
#ifdef RAM_DEBUG
			message_t *msg0 = msg;
			uint8_t id = get_type(msg);
			uint8_t dst = get_dst(msg);
			uint8_t len = get_payload_len(msg);
#endif

			msg = signal Receive.receive[get_type(msg)](msg, get_payload(msg), 
					get_payload_len(msg)); /* user may return a different buffer */
			
			if (src != myaddr()) {
				m_buf_freelist[m_buf_nfree++] = msg;
			}
			
			ram_debug_msg_with_info("deliver", msg0, id, src, dst, len, "0x%08x", (unsigned) msg);
		}

		if (!call deliver_queue.empty()) {
			post signal_deliver();
		}
	}

	task void send_msg() {
		if (!call send_queue.empty()) {
			message_t *msg = call send_queue.head();
			am_addr_t dst = get_dst(msg);	
			error_t error = SUCCESS;
			am_addr_t next_hop = INVALID_HOST_ADDR; /* to suppress compiler warning */

#ifdef RAM_DEBUG
			uint8_t id = get_type(msg);
			am_addr_t src = get_src(msg);
			uint8_t len = call SubPacket.payloadLength(msg);
			uint8_t dis0, dis1, dis2;
			call Routing.get_distance(0, &dis0);
			call Routing.get_distance(1, &dis1);
			call Routing.get_distance(2, &dis2);
#endif

			if (SUCCESS != call Routing.get_next_hop(dst, &next_hop) ||
				next_hop == INVALID_HOST_ADDR) {
				error = FAIL;
			}

			call PacketAcknowledgements.noAck(msg);

			if (error != SUCCESS ||
					SUCCESS != (error = 
					call SubSend.send(next_hop, msg, call SubPacket.payloadLength(msg)))) {
				signal AMSend.sendDone[get_type(msg)](msg, error);
				call send_queue.dequeue();
				if (!call send_queue.empty()) {
					post send_msg();
				}
			}

			ram_debug_msg_with_info("send_msg", msg, id, src, dst, len, "%u %u\t%u %u %u",
					next_hop,error, dis0, dis1, dis2);
		}
	}

	void handle_send_done(message_t *msg, error_t error) {
		am_addr_t src;
		
		if (msg != call send_queue.head()) return ;
		call send_queue.dequeue();

		src = get_src(msg);
		if (src == myaddr()) {
			signal AMSend.sendDone[get_type(msg)](msg, error);
		}
		else {
			m_buf_freelist[m_buf_nfree++] = msg;
		}
		
		if (!call send_queue.empty()) {
			post send_msg();
		}
	}


	void deliver_msg(message_t *msg) {
		if (SUCCESS == call deliver_queue.enqueue(msg)) {
			if (call deliver_queue.size() == 1) {
				post signal_deliver();
			}
		}
	}

	error_t cancel(message_t *msg_to_cancel) {
		message_t *msg, *msg0;
		if (call send_queue.empty()) return FAIL;
		if (call send_queue.size() == call send_queue.maxSize()) {
			return FAIL;
		}
		
		if ((msg = msg0 = call send_queue.dequeue()) == msg_to_cancel) {
			return call SubSend.cancel(msg);
		}
		else {
			call send_queue.enqueue(msg);
		}

		while (call send_queue.head() != msg0) {
			msg = call send_queue.dequeue();
			if (msg == msg_to_cancel) {
				while (call send_queue.head() != msg0) {
					call send_queue.enqueue(call send_queue.dequeue());	
				}
				call cancel_queue.enqueue(msg_to_cancel);
				if (call cancel_queue.size() == 1)
					post signal_cancel_done();
				return SUCCESS;
			}
		}

		return FAIL;
	}

	error_t _send(message_t *msg) {
		am_addr_t dst = get_dst(msg);

		if (dst == myaddr()) {
			deliver_msg(msg);
			return SUCCESS;	
		}
		else {
			uint8_t size = call send_queue.size();
			if (size == call send_queue.maxSize())
				return FAIL;

			call send_queue.enqueue(msg);
			if (size == 0)
				post send_msg();

			return SUCCESS;
		}
	}

	error_t send(uint8_t id, am_addr_t dst, message_t *msg, uint8_t len) {
		if (len > get_max_payload_len()) return FAIL;

#ifdef RAM_DEBUG
		{
			uint8_t src = myaddr();
			ram_debug_msg("send", msg, id, src, dst, len);
		}
#endif
		set_src_dst(msg, myaddr(), dst);
		set_payload_len(msg, len);
		set_type(msg, id);

		return _send(msg);
	}

	message_t *forward(message_t *msg) {
		message_t *msg_buf;
		
		if (call send_queue.size() == call send_queue.maxSize()) {
			return msg;
		}

#ifdef RAM_DEBUG 
		{
			uint8_t id = get_type(msg);
			am_addr_t src = get_src(msg);
			am_addr_t dst = get_dst(msg);
			uint8_t len = get_payload_len(msg);
			
			am_addr_t from = (src == myaddr()) ? src : (call SubAMPacket.source(msg));

			ram_debug_msg_with_info("forward", msg, id, src, dst, len, "%u", from);
		}
#endif

		msg_buf = m_buf_freelist[--m_buf_nfree];
		if (SUCCESS != _send(msg)) {
			m_buf_freelist[m_buf_nfree++] = msg;
		}

		return msg_buf;
	}

	event void SubSend.sendDone(message_t *msg, error_t error) {
		handle_send_done(msg, error);
	}

	event message_t *SubReceive.receive(message_t *msg, void *payload, uint8_t len) {
		return forward(msg);	
	}

/* interface AMPacket 
 XXX no proper support for group */

	command am_addr_t AMPacket.address() {
		return myaddr();
	}

	command am_addr_t AMPacket.destination(message_t *msg) {
		return get_dst(msg);
	}

	command am_group_t AMPacket.group(message_t *msg) {
		return call SubAMPacket.group(msg);
	}

	command bool AMPacket.isForMe(message_t *msg) {
		return get_dst(msg) == myaddr();
	}

	command am_group_t AMPacket.localGroup() {
		return call SubAMPacket.localGroup();
	}

	command void AMPacket.setDestination(message_t *msg, am_addr_t addr) {
		set_dst(msg, addr);
	}

	command void AMPacket.setGroup(message_t *msg, am_group_t grp) {
		call SubAMPacket.setGroup(msg, grp);
	}

	command void AMPacket.setSource(message_t *msg, am_addr_t addr) {
		set_src(msg, addr);
	}

	command void AMPacket.setType(message_t *msg, am_id_t t) {
		set_type(msg, t);
	}

	command am_addr_t AMPacket.source(message_t *msg) {
		return get_src(msg);
	}

	command am_id_t AMPacket.type(message_t *msg) {
		return get_type(msg);
	}

/* Packet interface */
	command void Packet.clear(message_t *msg) {
		call SubPacket.clear(msg);
	}

	command void *Packet.getPayload(message_t *msg, uint8_t len) {
		if (len > get_max_payload_len()) return NULL;
		return get_payload(msg);
	}

	command uint8_t Packet.maxPayloadLength() {
		return get_max_payload_len();
	}

	command uint8_t Packet.payloadLength(message_t *msg) {
		return get_payload_len(msg);			
	}

	command void Packet.setPayloadLength(message_t *msg, uint8_t len) {
		set_payload_len(msg, len);
	}

/* AMSend interface */

	command error_t AMSend.cancel[uint8_t id](message_t *msg) {
		return cancel(msg);
	}

	command void *AMSend.getPayload[uint8_t id](message_t *msg, uint8_t len) {
		if (len > get_max_payload_len()) return NULL;
		return get_payload(msg);
	}

	command uint8_t AMSend.maxPayloadLength[uint8_t id]() {
		return get_max_payload_len();
	}

	command error_t AMSend.send[uint8_t id](am_addr_t addr, message_t *msg, uint8_t len) {
		return send(id, addr, msg, len);
	}

	default event void AMSend.sendDone[uint8_t id](message_t *msg, error_t error) {}

/* Receive interface */

	default event message_t *Receive.receive[uint8_t id](message_t *msg,
			void *payload, uint8_t len) {
		return msg;
	}

/* SplitControl interface */
	command error_t SplitControl.start() {
		return call SubSplitControl.start();
	}

	command error_t SplitControl.stop() {
		stop();
		call Routing.stop();
		return call SubSplitControl.stop();
	}

	event void SubSplitControl.startDone(error_t error) {
		if (error == SUCCESS) {
			init();
			call Routing.init();
		}
		signal SplitControl.startDone(error);
	}

	event void SubSplitControl.stopDone(error_t error) {
		if (error == SUCCESS) {
		}
		/* TODO ?? */
		signal SplitControl.stopDone(error);
	}
}
