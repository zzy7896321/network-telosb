Sensor Network
=====================
SJTU network course project (spring 2015)

We implemented a multi-hop routing protocol and a sensor data collection
application based on that. The sensor network may consist of up to 16 nodes, 
one of which is designated as the sink node while all the others send sensor
data to the sink periodically. The sink forward the data through serial port
to the computer. A command line parser will display the data in human-readable
form.

## Compile and Run

### Makefile and Variables

set "BASEDIR" to the root directory and include $BASEDIR/Makefile.global

add -DNET_TELOSB_DEBUG to CFLAGS to the enable debug. uncomment RAM_DEBUG
and ROUTING_DEBUG in config.h to enable debug of routing components. 

NOTE: components will emit a lot of messages using PrintfC when debug is 
enabled, which may cause the mote to stop sending/receiving messages.

All source files in this project should include config.h before any code.

### Scripts

Make and install app to a telosb mote:

	scripts/mote-install <usbid> <am_addr>

Start net.tinyos.tools.PrintfClient:

	scripts/mote-printflisten <usbid>

Start net.tinyos.tools.Listen

	scripts/mote-listen

## Routing

See routing/README.md

## Sensor application

See apps/README.md

## Build system


