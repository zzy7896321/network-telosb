import sys

try:
    while True:
        line = sys.stdin.readline().strip("\r\n ").split(' ')
        src = int("0x%s" % (str(line[3]) + str(line[4])), 16)
        temp = int("0x%s" % (str(line[-6]) + str(line[-5])), 16) / 100.0 - 39.6
        humid = int("0x%s" % (str(line[-4]) + str(line[-3])), 16)
        humid = humid * 0.0405 - 4 - 0.0000028 * humid * humid
        light = int("0x%s" % (str(line[-2]) + str(line[-1])), 16) / 4096.0 * 2.5 * 6250
        print "%d\t%.2f\t%.2f\t%.2f" % (src, temp, humid, light)
except:
    pass
