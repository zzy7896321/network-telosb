#include "config.h"

module InitDebugP {
	uses interface Boot;
	uses interface SplitControl as SerialAMControl;
}
implementation {
	
	event void Boot.booted() {
		call SerialAMControl.start();
	}

	event void SerialAMControl.startDone(error_t error) {
		debug("SerialAM startDone");
	}

	event void SerialAMControl.stopDone(error_t error) {
	}
}
