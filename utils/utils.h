#ifndef UTILS_H
#define UTILS_H

#define _offsetof(type, field)	(uintptr_t) (&(((type *) 0)->field))

#define static_assert_stmt(expr, msg)	\
	sizeof(struct { int assertion_failure[(expr) ? 1 : -1]; })

#define static_assert_decl(expr, msg)	\
	static void assertion_failure(int _unused_var[][(expr) ? 1 : -1])	\
	__attribute__((unused))

#define QUEUE_GO_NEXT(i, qsize) ((void) ((++i == ((qsize) + 1)) && (i = 0)))
#define QUEUE_GO_PREV(i, qsize) ((void) ((i == 0) ? (i = (qsize)) : (--i)))
#define QUEUE_NEXT(i, qsize) ((i == (qsize)) ? 0 : (i + 1))
#define QUEUE_PREV(i, qsize) ((i == 0) ? (qsize) : (i - 1))


#endif
