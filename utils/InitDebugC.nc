#include "config.h"

configuration InitDebugC {
}
implementation {
	components InitDebugP, MainC;
	components SerialActiveMessageC;

	InitDebugP.Boot -> MainC;
	InitDebugP.SerialAMControl -> SerialActiveMessageC;
}
